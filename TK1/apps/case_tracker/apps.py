from django.apps import AppConfig


class CaseTrackerConfig(AppConfig):
    name = 'case_tracker'
