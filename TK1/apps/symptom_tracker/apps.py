from django.apps import AppConfig


class SymptomTrackerConfig(AppConfig):
    name = 'symptom_tracker'
