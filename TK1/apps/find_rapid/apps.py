from django.apps import AppConfig


class FindRapidConfig(AppConfig):
    name = 'find_rapid'
